<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S5 A1</title>
</head>
<body>
	<?php session_start(); ?>

	<form method="POST" action="./server.php">
		username: <input type="email" name="username" required>
		password: <input type="password" name="password" required>
		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['username'])): ?>

		<p>Hello, <?= $_SESSION['username']; ?></p>

	<?php else: ?>

		<?php if(isset($_SESSION['login_error'])): ?>

			<p><?= $_SESSION['login_error']; ?></p>

		<?php endif;?>	

	<?php endif;?>
</body>
</html>
